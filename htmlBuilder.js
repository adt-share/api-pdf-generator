const FONT_STYLE = 'font-family: "Helvetica Neue",Trebuchet MS, sans-serif;font-size: 12px;color: #444';
const fs = require('fs');
let config = {};
config = JSON.parse(fs.readFileSync(".config"));

function getStyling(customCss) {
    return `<style>
        /* styling for looks */
        html, body {
            width: 210mm;
            height: 297mm;
        }
        body {
            ${FONT_STYLE}
        }
        p {
            ${FONT_STYLE}
        }
        .alternate-row {
            background-color: #EAEAEA;
        }
        .header-small-heading {
            font-weight: bold;
            float: right;
        }
        .small-heading {
            font-weight: bold;
        }
        .div-container-heading-summ {
            margin-left: 20px;
        }
        .subheading-text {
            font-size: 98%;
            color: #555;
        }
        .th-heading-big {
            width:60%;
            text-align:left;
        }
        .th-heading {
            width:30%;
            text-align:left;
        }
        .th-heading-small {
            width:10%;
            text-align:left;
        }
        .table-info-footer {
            width:100%;
            font-size:100%;
            text-align: center;
            background-color: #f2f2f2;
            border: 1px solid white;
            border-collapse: collapse;
        }
        .td-info {
            width:50%;
            border: 1px solid white;
            border-collapse: collapse;
            padding: 10px!important:
        }
        .td-info-small {
            width:25%;
            border: 1px solid white;
            border-collapse: collapse;
            padding: 10px!important;
        }
        .table-margin {
            width:100%;
            font-size:100%;
            margin-top:0px;
        }
        .path-table-container {
            margin-left:30px;
            margin-right:30px;
        }
        .table-std {
            width:100%;
        }
        .div-container-margin {
            margin-left:21px;
            margin-right:51px;
            margin-bottom:4em;
            page-break-inside:avoid;
        }
        .td-alignment-small {
            vertical-align:top;
            width:20%;
        }
        .td-alignment-std {
            vertical-align:top;
            width:80%;
        }
        .td-alignment-small-no-width {
            vertical-align:top;
        }
        .td-alignment-std-no-width {
            vertical-align:top;
        }
        h3.get {
            background-color: #0f6ab4;
            border-bottom: 1px solid #eee;
        }
        h3.post {
            background-color: #10a54a;
            border-bottom: 1px solid #eee;
        }
        h3.patch,
        h3.put {
            background-color: #c5862b;
            border-bottom: 1px solid #eee;
        }
        h3.delete {
            background-color: #a41e22;
            border-bottom: 1px solid #eee;
        }

        li span.get {
            color: #0f6ab4;
        }
        li span.post {
            color: #10a54a;
        }
        li span.patch,
        li span.put {
            color: #c5862b;
        }
        li span.delete {
            color: #a41e22;
        }

        h3 {
            padding: 10px;
            margin: 0 30px 2px 30px;
            color:#FFF;
            font-size:100%;
        }
        td {
            padding-top:4px;
            padding-bottom:4px;
            padding-right: 4px;
            padding-left:2px;
        }
        .full-wrap {
            overflow-wrap: break-word;
            word-wrap: break-word;

            -ms-word-break: break-all;
            /* This is the dangerous one in WebKit, as it breaks things wherever */
            word-break: break-all;
            /* Instead use this non-standard one: */
            word-break: break-word;

            /* Adds a hyphen where the word breaks, if supported (No Blink) */
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }
        .page {
            page-break-after:always;
            position: fixed;
        }
        .centerAlign {
            text-align:center;
        }
        .title{
            text-align: center;
        }
        .coverHeadings {
            color: #0f6ab4;
        }
        .header, .header-space,
        .footer, .footer-space {
          height: 50px;
        }
        .header {
          position: fixed;
          margin: auto;
          width: 210mm;
          top: 0;
        }
        .footer {
          position: fixed;
          margin: auto;
          width: 210mm;
          bottom: 0;
        }
        .moddedHR {
            border: none;
            height: 2px;
            color: #0f6ab4;
            background-color: #0f6ab4;
        }
        list[type=bullet] item description:before{
            content:"\\a";
            white-space:pre;
        }
        li a{
            text-decoration:none;
        }
        li span{
            display:inline-block;
            width:4em;
        }
        h3 span,
        li span {
            font-size:13px;
            font-weight:bolder;
            text-transform:uppercase;
        }
        h3 span:after,
        li span:after {
            content:' '
        }
        .blue-text {
            color: #2e74b5;
        }
        .text-left {
            text-align: left;
            padding-left: 5px;
        }
        .text-right {
            text-align: right;
            padding-right: 10px;
        }
        .distribuzione {
            vertical-align: top;
            border-right: 1px solid black;
            border-collapse: collapse;
        }
        .center {
            margin: auto;
            margin-top: 20px;
            width: 50%;
        }
        .content {
            margin: auto;
            width: 210mm;
        }
        @page {
            size: ${config.paperSize.format};
            margin: ${config.paperSize.border};
          }
          
          @media print {
            .footer {
              position: fixed;
              width: 100%;
              bottom: 0;
            }
            .header {
                position: fixed;
                width: 100%;
                top: 0;
            }         
            html, body {
              width: 210mm;
              height: 297mm;
            }
          }
    </style>
    <style>
        /* styling to generate counters */
        body {
            counter-reset: h2counter;
        }
        h1 {
            counter-reset: h2counter;
        }
        h2:not(.subtitle){
            counter-reset: h3counter;
        }
        h2:not(.subtitle)::before {
            counter-increment: h2counter;
            content: counter(h2counter) ". ";
        }
        h3:not(.subtitle) {
            counter-reset: h4counter;
        }
        h3:not(.subtitle)::before {
            counter-increment: h3counter;
            content: counter(h2counter) "." counter(h3counter) ". ";
        }
        h4::before {
            counter-increment: h4counter;
            content: counter(h2counter) "." counter(h3counter) "." counter(h4counter) ". ";
        }

        li { 
            display: block;
            font-size:12px; 
        }
        ul>li{ 
            margin-left:1em;
            font-size:13px;
        }
        ol { counter-reset: item; }
        ol li:before { 
            content: counters(item, ".") " "; 
            counter-increment: item;
            margin-right:1em;
            display:inline-block;
            width:2em;
        }
    </style>
    `;
}

function genDate() {
    var data = new Date();
    var gg, mm, aaaa;
    gg = data.getDate();
    mm = data.getMonth() + 1;
    aaaa = data.getFullYear();
    return gg + "/" + mm+ "/" + aaaa;
};

function getFooter() {
    let info = {};
    info = JSON.parse(fs.readFileSync(".info"));
    let date = genDate();
    return `
    <table class='table-info-footer'>
        <tr>
            <td class='td-info-small text-left'>Versione ${info.versione} <br />del ${info.datacreazionedocumento}</td>
            <td class='td-info'>${info.rti} ${info.confidenziale ? "<br /><span class='blue-text'>Uso Confidenziale</span>" : ""}</td>
            <td class='td-info-small'>&nbsp;</td>
        </tr>
    </table>    
    `;
}

function getHeader() {
    let info = {};
    info = JSON.parse(fs.readFileSync(".info"));
    return `
    <table class='table-info-footer'>
        <tr>
            <td class='td-info text-left full-wrap'>${info.rti}</td>
            <td class='td-info text-right'>${info.lottospc}</td>
        </tr>
        <tr>
            <td class='td-info text-left'>${info.tipodocumento}</td>
            <td class='td-info text-right'><span class='blue-text'>${info.nomefile}</span></td>
        </tr>
    </table>    
    `;
}

function getImagesTable() {
    return `
    <table width="100%"><tr height="200px">
        <td width="10%"></td>
        <td width="16%" class='almaviva-img'></td>
        <td width="5%"></td>
        <td width="16%" class='almawave-img'></td>
        <td width="5%"></td>
        <td width="16%" class='indra-img'></td>
        <td width="5%"></td>
        <td width="16%" class='pwc-img'></td>
        <td width="10%"></td>
    </tr></table>
    `;
}

function getTitlePage(swaggerJSON) {
    let info = {};
    info = JSON.parse(fs.readFileSync(".info"));
    let images = getImagesTable();
    let lista = "";
    for (name in info.listadistribuzione) {
        lista += info.listadistribuzione[name];
        lista += "<br />";
    }
    return `<div style='page-break-after:always'>
                <div>
                    ${images}
                </div>
                <div class='${info.lottospcimgclass}'></div>
                <div class='title'>
                    <h1>${info.progetto}</h1>
                    <h1 class='subtitle'>${info.area}</h1>
                    <h2 class='subtitle'>${info.tipodocumento}</h2>
                    <h3 class='subtitle blue-text'>${info.lottospc}</h3>
                </div>
                <div class='center'>
                    <table width='100%' style='border-collapse: collapse'>
                        <tr>
                            <td class='td-info text-right distribuzione'>Compilato</td>
                            <td class='td-info text-left'>${info.compilato}</td>
                        </tr>
                        <tr>
                            <td class='td-info text-right distribuzione'>Rivisto</td>
                            <td class='td-info text-left'>${info.rivisto}</td>
                        </tr>
                        <tr>
                            <td class='td-info text-right distribuzione'>Approvato</td>
                            <td class='td-info text-left'>${info.approvato}</td>
                        </tr>
                        <tr>
                            <td class='td-info text-right distribuzione'>Lista di distribuzione</td>
                            <td class='td-info text-left'>${lista}</td>
                        </tr>
                    </table>
                </div>
            </div>`;
            
                //<div class='footer'>Generated on:  ${new Date()}</div>
}

function getIntroduction(swaggerJSON) {
    if (swaggerJSON.info.description != null) {
        return `<div style='page-break-after:always'>
                    <h1 class='coverHeadings'>Introduzione</h1>
                    <div>${swaggerJSON.info.description.replace('\n\n', '<br />')}</div>
                </div>`;
    }
    return '';
}

function getTableOfContent(swaggerJSON) {
    let htmlToc = '';

    const tags = [...new Set(swaggerJSON.tags.map(item => item.name))];
    const paths = tags
        .map(tag => {
            return (
                `<li><b>${tag}</b><ol>` +
                getPathsByTag(swaggerJSON, tag)
                    .map(
                        path =>
                            `<li><!--a href="#${path.action}${path.path}"--><span class="${path.action}">${
                                path.action
                            }</span>${path.path}<!--/a--></li>`
                    )
                    .join(' ') +
                '</ol></li>'
            );
        })
        .join(' ');

    htmlToc += '<li><b>Endpoints</b><ol>' + paths + '</ol></li>';

    if (swaggerJSON.definitions && Object.keys(swaggerJSON.definitions).length) {
        //start a new chapter
        htmlToc +=
            '<li><b>Definizioni</b><ol>' +
            Object.keys(swaggerJSON.definitions)
                .filter(key => key !== 'Array')
                .map(definition => `<li><!--a href="#${definition}"-->${definition}<!--/a--></li>`)
                .join(' ') +
            //end the chapter
            '</ol></li>';
    }
    return `<div style='page-break-after:always'>
                <h1 class='coverHeadings'>Sommario</h1>
                <ul>
                ${htmlToc}
                </ul>
            </div>`;
}

function getPathsByTag(swagger, tag) {
    return Object.keys(swagger.paths)
        .map(path =>
            Object.keys(swagger.paths[path])
                .map(action =>
                    swagger.paths[path][action].tags.includes(tag)
                        ? { action, path, spec: swagger.paths[path][action] }
                        : null
                )
                .filter(x => !!x)
        )
        .reduce((acc, val) => acc.concat(val), []);
}

function getPathSpecifications(swaggerJSON) {
    const tags = [...new Set(swaggerJSON.tags.map(item => item.name))];
    const paths = tags
        .map(tag => {
            return (
                `<div style='page-break-after:always'>` +
                `<h2>${tag}</h2>` +
                getPathsByTag(swaggerJSON, tag)
                    .map(path => {
                        let html = '';
                        html += '<div class="path-table-container">'; // path start
                        html += "<table class='table-margin'>";

                        // summary
                        html += '    <tr>';
                        html += "           <td class='td-alignment-small'><b>Riepilogo</b></td>";
                        html +=
                            "           <td class='td-alignment-std full-wrap'>" +
                            (typeof path.spec.summary !== 'undefined' ? path.spec.summary : '') +
                            '</td>';
                        html += '    </tr>';

                        // description
                        html += "    <tr class='alternate-row'>";
                        html += "           <td class='td-alignment-small'><b>Descrizione</b></td>";
                        html +=
                            "           <td class='td-alignment-std full-wrap'>" +
                            (typeof path.spec.description !== 'undefined' ? path.spec.description : '') +
                            '</td>';
                        html += '    </tr>';

                        // operationId
                        html += '    <tr>';
                        html += "           <td class='td-alignment-small'><b>Id Operazione</b></td>";
                        html +=
                            "           <td class='td-alignment-std full-wrap'>" +
                            (typeof path.spec.operationId !== 'undefined' ? path.spec.operationId : '') +
                            '</td>';
                        html += '    </tr>';

                        // action produces
                        html += "    <tr class='alternate-row'>";
                        html += "           <td class='td-alignment-small'><b>Produce</b></td>";
                        html +=
                            "           <td class='td-alignment-std full-wrap'>" +
                            (typeof path.spec.produces !== 'undefined' ? path.spec.produces.join(' ') : '') +
                            '</td>';
                        html += '    </tr>';

                        // action consumes
                        html += '    <tr>';
                        html += "           <td class='td-alignment-small'><b>Consuma</b></td>";
                        html +=
                            "           <td class='td-alignment-std full-wrap'>" +
                            (typeof path.spec.consumes !== 'undefined' ? path.spec.consumes.join(' ') : '') +
                            '</td>';
                        html += '    </tr>';

                        // action params
                        html += '    <tr>';
                        html += "           <td class='td-alignment-small'><b>Parametri</b></td>";
                        html += "           <td class='td-alignment-std'>";

                        if (typeof path.spec.parameters !== 'undefined') {
                            html += "<table class='table-margin'>";
                            html += '   <thead>';
                            html += '     <tr>';
                            html += "       <td class='small-heading'>Nome</td>";
                            html += "       <td class='small-heading'>In</td>";
                            html += "       <td class='small-heading'>Descrizione</td>";
                            html += "       <td class='small-heading'>Obb.</td>";
                            html += "       <td class='small-heading'>Tipo</td>";
                            html += "       <td class='small-heading'>Formato</td>";
                            html += "       <td class='small-heading'>Formato Collezione</td>";
                            html += "       <td class='small-heading'>Default</td>";
                            html += "       <td class='small-heading'>Min</td>";
                            html += "       <td class='small-heading'>Max</td>";
                            html += '    </tr>';
                            html += '   </thead>';

                            html += '   <tbody>';
                            for (let paramIndex = 0; paramIndex < path.spec.parameters.length; paramIndex++) {
                                html += `   <tr ${paramIndex % 2 == 0 ? 'class="alternate-row"' : ''}'>`;
                                let param = path.spec.parameters[paramIndex];

                                // name
                                html += "       <td class='td-alignment-small-no-width'>" + param.name + '</td>';

                                // in
                                html += "       <td class='td-alignment-small-no-width'>" + param.in + '</td>';

                                // description
                                let paramDescription = param.description || '';
                                if (
                                    typeof param.schema !== 'undefined' &&
                                    typeof param.schema['$ref'] !== 'undefined'
                                ) {
                                    let dfn = param.schema['$ref'].split('/');
                                    paramDescription +=
                                        '<br />' + renderDefinition(true, dfn[dfn.length - 1], swaggerJSON.definitions);
                                }
                                if (!paramDescription) {
                                    paramDescription = '&nbsp;';
                                }
                                html += "       <td class='td-alignment-small-no-width'>" + paramDescription + '</td>';

                                // required
                                html +=
                                    "       <td class='td-alignment-small-no-width'>" +
                                    (typeof param.required !== 'undefined'
                                        ? param.required == true
                                            ? 'Si'
                                            : 'No'
                                        : 'No') +
                                    '</td>';

                                // type
                                if (param.type == 'array' && param.items != null && param.items.type != null) {
                                    html +=
                                        "       <td class='td-alignment-small-no-width'>" +
                                        'array of ' +
                                        param.items.type +
                                        '</td>';
                                } else {
                                    html +=
                                        "       <td class='td-alignment-small-no-width'>" +
                                        (typeof param.type !== 'undefined' ? param.type : '') +
                                        '</td>';
                                }

                                // format
                                html +=
                                    "       <td class='td-alignment-small-no-width full-wrap'>" +
                                    (typeof param.format !== 'undefined' ? param.format : '') +
                                    '</td>';

                                // collection format
                                html +=
                                    "       <td class='td-alignment-small-no-width full-wrap'>" +
                                    (typeof param.collectionFormat !== 'undefined' ? param.collectionFormat : '') +
                                    '</td>';

                                // default
                                html +=
                                    "       <td class='td-alignment-small-no-width full-wrap'>" +
                                    (typeof param.default !== 'undefined' ? param.default : '') +
                                    '</td>';

                                // minimum
                                html +=
                                    "       <td class='td-alignment-small-no-width full-wrap'>" +
                                    (typeof param.minimum !== 'undefined' ? param.minimum : '') +
                                    '</td>';

                                // maximum
                                html +=
                                    "       <td class='td-alignment-small-no-width full-wrap'>" +
                                    (typeof param.maximum !== 'undefined' ? param.maximum : '') +
                                    '</td>';
                                html += '   </tr>';
                            }
                            html += '   </tbody>';
                            html += '   </table>';
                        } else {
                            html += '<p>' + 'no parameters' + '</p>';
                        }

                        // tags
                        if (typeof path.spec.tags !== 'undefined') {
                            html += '    <tr>';
                            html += "           <td class='td-alignment-small'><b>Tags</b></td>";
                            html +=
                                "           <td class='td-alignment-std' style='padding-left:6px'>" +
                                path.spec.tags.join(' ') +
                                '</td>';
                            html += '    </tr>';
                        } else {
                            // no tags
                        }

                        // action security
                        if (typeof path.spec.security !== 'undefined') {
                            html += '<tr>';
                            html += "<td class='td-alignment-small'><b>Sicurezza</b></td>";
                            html +=
                                "<td class='td-alignment-std' style='padding-left:0px!important;margin-left:0px!important'>";

                            // response schema start
                            html += "<table class='table-margin' style='width:42%'>";

                            html += '   <tr>';
                            for (let securityIndex = 0; securityIndex < path.spec.security.length; securityIndex++) {
                                let security = path.spec.security[securityIndex];
                                let iSec = 0;
                                for (let securityItem in security) {
                                    html +=
                                        "<td class='td-alignment-small'><b>" +
                                        securityItem +
                                        '</b> (' +
                                        path.spec.security[securityIndex][securityItem].join(', ') +
                                        ')' +
                                        '</td>';
                                }
                            }
                            html += '       </td>';
                            html += '   </tr>';
                            html += '   </table>';
                        } else {
                            // no security
                        }

                        // action responses
                        html += '      <tr>';
                        html += "           <td class='td-alignment-small'><b>Responses</b></td>";
                        html += "           <td class='td-alignment-std' style='padding-left:0px!important;margin-left:0px!important'>";
                        html += "      </tr><tr>";
                        html += "           <td colspan='2' style='padding:0px!important;margin:0px!important'>";
                        // response schema start
                        html += "<table>";

                        html += '   <tr>';
                        html += "       <td class='td-alignment-small'><b>Codice</b></td>";
                        html += "       <td class='td-alignment-std'><b>Descrizione</b></td>";
                        html += '   </tr>';
                        for (let response in path.spec.responses) {
                            // eg 200

                            // response schema start
                            html += '   <tr>';
                            html += "       <td class='td-alignment-small'>" + response + '</td>';
                            html += "       <td class='td-alignment-std'>" + path.spec.responses[response].description;
                            html += '   </tr>';
                            let responseSchema = path.spec.responses[response].schema;

                            // response schema
                            html += '   <tr>';
                            html += '   <td colspan="2">';
                            //let hasResponseSchema = false;
                            let responseSchemaHTML = '&nbsp;';
                            if (typeof responseSchema !== 'undefined') {
                                responseSchemaHTML += "       <table class='table-margin'>";
                                if (typeof responseSchema.type !== 'undefined') {
                                    responseSchemaHTML += '   <tr>';
                                    responseSchemaHTML += "       <td style='td-alignment-small'>&nbsp;</td>";
                                    responseSchemaHTML +=
                                        "       <td style='td-alignment-std'><b>Tipo Schema</b> " + responseSchema.type + '</td>';
                                    responseSchemaHTML += '   </tr>';
                                    hasResponseSchema = true;
                                }

                                // response schema items
                                let responseSchemaItems = responseSchema.items;
                                if (typeof responseSchemaItems !== 'undefined') {
                                    responseSchemaHTML += '   <tr>';
                                    responseSchemaHTML += "       <td class='td-alignment-small'>&nbsp;</td>";
                                    responseSchemaHTML +=
                                        "       <td class='td-alignment-std'>" +
                                        renderSchemaItems(responseSchemaItems, swaggerJSON.definitions) +
                                        '</td>';
                                    responseSchemaHTML += '   </tr>';
                                    hasResponseSchema = true;
                                } else {
                                    responseSchemaHTML += '   <tr>';
                                    responseSchemaHTML += "       <td class='td-alignment-small'>&nbsp;</td>";
                                    responseSchemaHTML +=
                                        "       <td class='td-alignment-std'>" +
                                        renderSchemaItems(responseSchema, swaggerJSON.definitions) +
                                        '</td>';
                                    responseSchemaHTML += '   </tr>';
                                    hasResponseSchema = true;
                                }
                                responseSchemaHTML += '       </table>';
                            }
                            html += responseSchemaHTML;

                            html += '       </td>';
                            html += '   </tr>';
                        }
                        html += '</table>'; //responses
                        html += '           </td>';
                        html += '    </tr>';
                        html += '</table></div>'; //TABLE FOR PATH END
                        return `<div style="page-break-after:always">
                                <h3 id="${path.action}${path.path}" class="${path.action}"><code class="huge"><span>${
                            path.action
                        }</span></code>${path.path}</h3> 
                                ${html}
                            </div>`;
                    })
                    .join(' ') +
                '</div>'
            );
        })
        .join(' ');

    return `<div>
                <h1 class='coverHeadings'>Endpoints</h1>
                ${paths}
            </div>`;
}

function renderDefinition(minimal, dfn, swaggerJSONdefinitions) {
    let html = ` 
    <table class='table-margin'>
       <thead>
            <tr>
                <th class='th-heading'><b>Nome</b></td>
                <th class='th-heading-small'><b>Tipo</b></td>
                ${minimal ? "<th class='th-heading-big'><b>Descrizione</b></td>" : "<th class='th-heading'><b>Descrizione</b></td>"}
                ${minimal ? '' : "           <th class='th-heading'><b>Obbligatorio</b></td>"}
            </tr>
       </thead>
       <tbody>`;

    if (swaggerJSONdefinitions[dfn].type === 'array') {
        html += "    <tr class='alternate-row'>";
        html += "           <td class='th-heading'></td>";
        html += "           <td class='th-heading-small'>array</td>";
        let description;
        if (typeof swaggerJSONdefinitions[dfn]['$ref'] !== 'undefined') {
            const items = swaggerJSONdefinitions[dfn]['$ref'].split('/');
            const subdfn = items[items.length - 1];
            description = `Vedi <b><!--a href='#${subdfn}'-->${subdfn}<!--/a--></b> nella sezione <b>Definizioni</b> per i dettagli.`;
        } else if (typeof swaggerJSONdefinitions[dfn]['items'] !== 'undefined') {
            if (typeof swaggerJSONdefinitions[dfn]['items'] === 'string') {
                const items = swaggerJSONdefinitions[dfn]['items'].split('/');
                const subdfn = items[items.length - 1];
                description = `Vedi <b><!--a href='#${subdfn}'-->${subdfn}<!--/a--></b> nella sezione <b>Definizioni</b> per i dettagli.`;
            } else if (typeof swaggerJSONdefinitions[dfn]['items'] === 'object') {
                if (typeof swaggerJSONdefinitions[dfn]['items']['$ref'] !== 'undefined') {
                    const items = swaggerJSONdefinitions[dfn]['items']['$ref'].split('/');
                    const subdfn = items[items.length - 1];
                    description = `Vedi <b><!--a href='#${subdfn}'-->${subdfn}<!--/a--></b> nella sezione <b>Definizioni</b> per i dettagli.`;
                }
            }
        }
        if (!minimal) {
            html += `           <td class='th-heading full-wrap'>${description}</td>`;
            html += "           <td class='th-heading'>yes</td>";
        } else {
            html += `           <td class='th-heading-big full-wrap'>${description}</td>`;
        }
        html += '       </tr>';
    } else {
        let index = 0;
        for (let dfnProps in swaggerJSONdefinitions[dfn].properties) {
            // eg: product_id
            html += `   <tr ${index % 2 == 0 ? 'class="alternate-row"' : ''}>`;
            html += "       <td class='th-heading'>" + dfnProps + '</td>';
            if (swaggerJSONdefinitions[dfn].properties[dfnProps] != null) {
                html +=
                    "       <td class='th-heading-small'>" +
                    (typeof swaggerJSONdefinitions[dfn].properties[dfnProps].type !== 'undefined'
                        ? swaggerJSONdefinitions[dfn].properties[dfnProps].type
                        : '') +
                    '</td>';
                    if (!minimal) {
                        html += "       <td class='th-heading full-wrap'>";
                    } else {
                        html += "       <td class='th-heading-big full-wrap'>";
                    }
                    if (typeof swaggerJSONdefinitions[dfn].properties[dfnProps]['$ref'] !== 'undefined') {
                        let items = swaggerJSONdefinitions[dfn].properties[dfnProps]['$ref'].split('/');
                        let subdfn = items[items.length - 1];
                        html += `Vedi <b><!--a href='#${subdfn}'-->${subdfn}<!--/a--></b> nella sezione <b>Definizioni</b> per i dettagli.`;
                    } else if (typeof swaggerJSONdefinitions[dfn].properties[dfnProps]['items'] !== 'undefined') {
                        if (typeof swaggerJSONdefinitions[dfn].properties[dfnProps]['items'] === 'string') {
                            let items = swaggerJSONdefinitions[dfn].properties[dfnProps]['items'].split('/');
                            let subdfn = items[items.length - 1];
                            html += `Vedi <b><!--a href='#${subdfn}'-->${subdfn}<!--/a--></b> nella sezione <b>Definizioni</b> per i dettagli.`;
                        } else if (typeof swaggerJSONdefinitions[dfn].properties[dfnProps]['items'] === 'object') {
                            if (
                                typeof swaggerJSONdefinitions[dfn].properties[dfnProps]['items']['$ref'] !== 'undefined'
                                ) {
                                    let items = swaggerJSONdefinitions[dfn].properties[dfnProps]['items']['$ref'].split(
                                        '/'
                                        );
                                        let subdfn = items[items.length - 1];
                                        html += `Vedi <b><!--a href='#${subdfn}'-->${subdfn}<!--/a--></b> nella sezione <b>Definizioni</b> per i dettagli.`;
                                    }
                                }
                            } else {
                                html +=
                                typeof swaggerJSONdefinitions[dfn].properties[dfnProps].description !== 'undefined'
                                ? swaggerJSONdefinitions[dfn].properties[dfnProps].description
                                : '';
                            }
                    html += '</td>';
                }
                
                let isRequired = false;
                if (swaggerJSONdefinitions[dfn].required != null) {
                    isRequired = swaggerJSONdefinitions[dfn].required.indexOf(dfnProps) !== -1;
                }
                if (!minimal) {
                    html += "       <td class='th-heading'>" + (isRequired == true ? 'Si' : 'No') + '</td>';
                }
            html += '   </tr>';
            
            index++;
        }
    }
    html += '   </tbody>';
    html += '</table>';
    return html;
}

function renderSchemaItems(schemaItems, swaggerDefinitions) {
    let html = '';

    if (typeof schemaItems['$ref'] !== 'undefined') {
        // eg: #/definitions/Product
        let items = schemaItems['$ref'].split('/');
        let dfn = items[items.length - 1];
        html += `Vedi <b><!--a href="#${dfn}"-->${dfn}<!--/a--></b> nella sezione <b>Definizioni</b> per i dettagli.`;
        html += '<br />';
        html += '<br />';

        html += renderDefinition(true, dfn, swaggerDefinitions);
    } else if (typeof schemaItems.type !== 'undefined') {
        html += 'Tipo oggetti: ' + schemaItems.type;
    }

    return html;
}

function getTypeDefinitions(swaggerJSON) {
    let definitions = Object.keys(swaggerJSON.definitions)
        .filter(key => key !== 'Array')
        .map(definition => {
            return `<div class="div-container-margin">
                    <h2 id="${definition}">${definition}</h2>
                    <hr />
                   ${renderDefinition(false, definition, swaggerJSON.definitions)}
                </div>`;
        })
        .join(' ');

    return `<div style='page-break-after:always'>
                <h1 class='coverHeadings'>Definizioni</h1>
                ${definitions}
            </div>`;
}

module.exports = {
    getStyling,
    getHeader,
    getTitlePage,
    getIntroduction,
    getTableOfContent,
    getPathSpecifications,
    getTypeDefinitions,
    getFooter
};
