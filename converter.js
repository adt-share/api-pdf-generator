const htmlBuilder = require('./htmlBuilder');
const images = require('./images');
function convertToHtml(swaggerJson, cssOverrides) {
    return `<html>
            ${htmlBuilder.getStyling(cssOverrides)} 
            ${images.getCssImages()}   
            <body>
            <table>
            <thead><tr><td>
                <div class="header-space">&nbsp;</div>
            </td></tr></thead>
            <tbody><tr><td>
                <div class="content">
                ${htmlBuilder.getTitlePage(swaggerJson)}
                ${htmlBuilder.getTableOfContent(swaggerJson)}
                ${htmlBuilder.getPathSpecifications(swaggerJson)}
                ${htmlBuilder.getTypeDefinitions(swaggerJson)}
                </div>
            </td></tr></tbody>
            <tfoot><tr><td>
                <div class="footer-space">&nbsp;</div>
            </td></tr></tfoot>
            </table>
            <div class="header">${htmlBuilder.getHeader()}</div>
            <div class="footer">${htmlBuilder.getFooter()}</div>
            </body>
            </html>`;
            //${htmlBuilder.getIntroduction(swaggerJson)}
}
        
module.exports = {
    convertToHtml
};
